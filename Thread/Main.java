/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lab1;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mac
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        Thread1 t1 = new Thread1();
        t1.start();
        
        MyRunnable r = new MyRunnable();
        Thread t2 = new Thread(r);
        t2.start();
//        
//        t1.join();
//        t2.join();
////        System.out.println(Thread.currentThread().getName());
//        System.out.println(Thread1.count[0]);

//        SynOnMethodDemo r = new SynOnMethodDemo();
//        SynOnMethodDemo t2 = new SynOnMethodDemo();
//        Thread t1 = new Thread(r);
//        t1.start();
//        r.run();
//        Thread t2 = new Thread(r);
//        t2.start();
//        t2.start();

        Class c = Main.class;
        synchronized(c) {}
        
        new Main().test1();
    }
    
    void test1() {
        System.out.println(this.getClass().getSimpleName());
    }
   
    static void test() {
        synchronized(Main.class) {
            for (int i = 0; i < 10; i++) {
                System.out.print(i + ", ");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(SynOnMethodDemo.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
