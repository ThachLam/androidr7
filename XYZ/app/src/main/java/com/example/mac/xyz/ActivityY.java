package com.example.mac.xyz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActivityY extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = ActivityY.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_y);

        txtT = (EditText) findViewById(R.id.editText);

        Button btnOK = (Button) findViewById(R.id.btnOK);
        btnOK.setOnClickListener(this);

        Log.d(TAG, "onCreate: " + this.hashCode());

        Log.d(TAG, "onCreate: " + this.getTaskId());
    }

    EditText txtT;

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick: ");
        Intent i = new Intent();
        i.putExtra("key", txtT.getText().toString());
        Bundle b = new Bundle();
        b.putParcelable("student", new Student(123, "ABC"));
        i.putExtra("bundle", b);

        setResult(RESULT_OK, i);
        finish();
    }
}
