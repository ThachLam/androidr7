package com.example.mac.xyz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActivityX extends AppCompatActivity implements View.OnClickListener {

    private EditText txtT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_x);


        Button btnOpenY = (Button) findViewById(R.id.openY);
        btnOpenY.setOnClickListener(this);

        txtT = (EditText) findViewById(R.id.editText);
    }

    @Override
    public void onClick(View v) {
//        startActivity(new Intent(this, ActivityY.class));
        startActivityForResult(new Intent(this, ActivityY.class), 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                txtT.setText(data.getStringExtra("key"));
            }
        }
    }
}
