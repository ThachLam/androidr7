package com.example.mac.abc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class Receiver2 extends BroadcastReceiver {

    public Receiver2() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("Receiver2", "onReceive: ");
        abortBroadcast();
    }
}
