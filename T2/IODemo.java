
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mac
 */
public class IODemo {
    public static void main(String[] args) {

//        byte b1 = 10;
//        byte b2 = 20;
//        byte b3 = b1 + b2;
        
//        try {
//            InputStream is = new FileInputStream("...");
//            OutputStream os = new FileOutputStream("...");
////            int byt = is.read();
//            byte[] b = new byte[4096];
//            while (is.available() > 0) {
//                int n = is.read(b);
//                System.out.println(new String(b, 0, n));
//                os.write(b, 0, n);
//            }
//            os.flush();
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(IODemo.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(IODemo.class.getName()).log(Level.SEVERE, null, ex);
//        }

        BufferedReader bf = null;
        try {
            bf = new BufferedReader(new InputStreamReader(new FileInputStream("...")));
            String line = "";
            while((line = bf.readLine()) != null) {
                System.out.println(line);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(IODemo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(IODemo.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (bf != null) {
                try {
                    bf.close();
                } catch (IOException ex) {
                    Logger.getLogger(IODemo.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        try (
            PrintWriter pw = new PrintWriter("...");
                // b
                // c
            ) {
            pw.write("ABC");
            pw.flush();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(IODemo.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("..."));
            oos.writeObject(new Student(0, "N"));
        } catch (IOException ex) {
            Logger.getLogger(IODemo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
