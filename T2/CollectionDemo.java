
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;
import java.util.Vector;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mac
 */
public class CollectionDemo {
    
    public static void main(String[] args) {
//        List a = new ArrayList();
//        Vector v = new Vector();
//        LinkedList lk = new LinkedList();
//        
//        a.add(new Student(1, "N1"));
//        a.add(new Student(2, "N2"));
//        System.out.println(a.contains(new Student(2, "N2")));

//        HashSet hs = new HashSet();
//        for (int i = 0; i < 100; i++) {
//            hs.add(new Student(i, "N" + (100 -i)));
//        }
//        
//        for (Object h : hs) {
//            System.out.println(h);
//        }

//        LinkedHashSet hs = new LinkedHashSet();
//        for (int i = 0; i < 100; i++) {
//            hs.add(new Student(i, "N" + (100 -i)));
//        }
//        
//        for (Object h : hs) {
//            System.out.println(h);
//        }

        TreeSet t = new TreeSet();
//        t.add("ABC");
//        t.add("ABB");
//        t.add("ABA");
//        t.add("AAC");
//        t.add("AAB");
//        for (Object o : t) {
//            System.out.println(o);
//        }

        for (int i = 0; i < 100; i++) {
            t.add(new Student(i, "N" + (100 -i)));
        }

        for (Object h : t) {
            System.out.println(h);
        }
    }
    
    
}

class Student implements Comparable<Student> {
    int ID;
    String name;

    public Student(int ID, String name) {
        this.ID = ID;
        this.name = name;
    }

    @Override
    public String toString() {
        return ID + ", " + name;
    }

    @Override
    public boolean equals(Object obj) {
        Student s = (Student) obj;
        return this.ID == s.ID && this.name.equals(s.name);
    }

    @Override
    public int hashCode() {
        return ID + name.hashCode();
    }

    @Override
    public int compareTo(Student o) {
        if (!this.name.equals(o.name)) {
            return this.name.compareTo(o.name);
        } else {
            return this.ID - o.ID;
        }
    }
}
